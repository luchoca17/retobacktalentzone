package com.example.demo.models;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Buy {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "buy_id", nullable = false)
    private Long buyId;
    private LocalDateTime date;
    private String clientName;
    @ManyToOne
    private User user;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Product> products;

}
