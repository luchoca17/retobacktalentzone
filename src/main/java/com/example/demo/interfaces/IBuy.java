package com.example.demo.interfaces;

import com.example.demo.dto.DTOBuy;
import com.example.demo.dto.DTOProduct;

import java.util.List;

public interface IBuy {


    //Registrar Compra
    DTOBuy createBuy(DTOBuy dtoBuy, DTOProduct dtoProduct);


    // Listar Compras
    List<DTOBuy> findAllBuys();


    //Editar Compra
    DTOBuy updateBuy(DTOBuy dtoBuy, Long buyId);


    //Eliminar Compra
    void deleteBuy(Long buyId);

}
