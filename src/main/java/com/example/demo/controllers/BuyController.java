package com.example.demo.controllers;


import com.example.demo.dto.DTOBuy;
import com.example.demo.dto.DTOProduct;
import com.example.demo.services.BuyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/buy")
public class BuyController {

    @Autowired
    BuyService buyService;

    @PostMapping("/createBuy")
    public ResponseEntity<DTOBuy> createBuy(@RequestBody DTOBuy dtoBuy, DTOProduct dtoProduct) {
        return ResponseEntity.ok().body(buyService.createBuy(dtoBuy, dtoProduct));
    }

    @GetMapping("/findAllBuys")
    public ResponseEntity<List<DTOBuy>> findAllBuys() {
        return ResponseEntity.ok().body(buyService.findAllBuys());
    }

    @DeleteMapping("/delete/{buyId}")
    public ResponseEntity<Void> deleteBuy(@PathVariable("buyId")Long buyId){
        buyService.deleteBuy(buyId);
        return null;
    }
}
