package com.example.demo.dto;

import com.example.demo.models.Buy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DTOUser {

    private Long userId;
    private String userName;
    private String email;
    private String idType;
    private List<Buy> buys;


}