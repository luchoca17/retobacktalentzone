package com.example.demo.services;

import com.example.demo.appUtils.AppUtils;
import com.example.demo.dto.DTOUser;
import com.example.demo.interfaces.IUser;
import com.example.demo.models.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


@Service
public class UserService implements IUser {
    @Autowired
    UserRepository userRepository;
    @Override
    public DTOUser createUser(DTOUser dtoUser) {
        User userCreated = userRepository.save(AppUtils.dtoToUser(dtoUser));

        return AppUtils.userToDto(userCreated);
    }

    @Override
    public List<DTOUser> findAllUser() {

        return userRepository.findAll().stream().map(AppUtils::userToDto).toList();
    }

    @Override
    public DTOUser updateUser(DTOUser dtoUser, Long userId) {
        var user = userRepository.findById(userId);//esto devuelve un Optional
        if (user.isPresent()){
            dtoUser.setUserId(userId);
            User updateUser = userRepository.save(AppUtils.dtoToUser(dtoUser));
        }
        return null;
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }
}
