package com.example.demo.interfaces;

import com.example.demo.dto.DTOProduct;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface IProduct {

    //Agregar/crear productos
    DTOProduct createProduct(DTOProduct dtoProduct);


    // Listar Productos
    List<DTOProduct> findAllProducts();


    //Editar Productos
    DTOProduct updateProduct(DTOProduct dtoProduct, Long productId);


    //Eliminar Productos
    void deleteProduct(Long productId);

}


//Crear Apis que permitan listar, agregar, editar y eliminar productos al inventario. El listado
//de productos debe contar con paginación y las respectivas validaciones para contratos de entrada de todo el CRUD.