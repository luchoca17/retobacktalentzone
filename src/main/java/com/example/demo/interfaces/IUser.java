package com.example.demo.interfaces;


import com.example.demo.dto.DTOUser;


import java.util.List;

public interface IUser {

    //Agregar/crear Usuario
    DTOUser createUser(DTOUser dtoUser);


    // Listar Usuario
    List<DTOUser> findAllUser();


    //Editar Usuario
    DTOUser updateUser(DTOUser dtoUser, Long userId);


    //Eliminar Usuario
    void deleteUser(Long userId);
    
}
