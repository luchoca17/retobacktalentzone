package com.example.demo.controllers;


import com.example.demo.dto.DTOProduct;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/createProduct")
    public ResponseEntity<DTOProduct> createProduct(@RequestBody DTOProduct dtoProduct){
        return ResponseEntity.ok().body(productService.createProduct(dtoProduct));
    }

    @GetMapping("/findAllProducts")
    public ResponseEntity<List<DTOProduct>> findAllProducts(){
        return ResponseEntity.ok().body(productService.findAllProducts());
    }

    @PutMapping("/updateProduct/{productId}")
    public ResponseEntity<DTOProduct> updateProduct(
            @RequestBody DTOProduct dtoProduct,
            @PathVariable Long productId){
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(productService.updateProduct(dtoProduct,productId));
    }

    @DeleteMapping("/delete/{productId}")
    public ResponseEntity<Void> deleteProduct(@PathVariable("productId")Long productId){
      productService.deleteProduct(productId);
        return null;
    }
}
