package com.example.demo.services;

import com.example.demo.appUtils.AppUtils;
import com.example.demo.dto.DTOBuy;
import com.example.demo.dto.DTOProduct;
import com.example.demo.interfaces.IBuy;
import com.example.demo.models.Buy;
import com.example.demo.repository.BuyRepository;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.List;

@Service
public class BuyService implements IBuy {


    @Autowired
    BuyRepository buyRepository;
    @Autowired
    ProductRepository productRepository;


    // Registrar / hacer compra
    @Override
    public DTOBuy createBuy(DTOBuy dtoBuy, DTOProduct dtoProduct) {
        dtoBuy.setDate(LocalDateTime.now());
        Buy buyRegister = buyRepository.save(AppUtils.dtoToBuy(dtoBuy));
        return AppUtils.buyToDto(buyRegister);
    }

    //Validar disponibilidad minimos y maximos
    //Descontar cantidades a los productos


    //historial de compras
    @Override
    public List<DTOBuy> findAllBuys() {
        return buyRepository.findAll().stream().map(AppUtils::buyToDto).toList();
    }


    //Actualizar compra
    @Override
    public DTOBuy updateBuy(DTOBuy dtoBuy, Long buyId) {
        return null;
    }

    //
    @Override
    public void deleteBuy(Long buyId) {
        buyRepository.deleteById(buyId);
    }
}




//Crear Apis de compras que permiten registrar la compra, la cual debe validar
//respectivamente la disponibilidad, los mínimos, máximos por producto, asentar la compra y
//descontar las cantidades compradas también debemos contar con un api para ver el
//historial de compras realizadas.
//Ejemplo.
//POST → /buys/
//{
//“date”: “current date with time”
// //“idType”: “CC”,
//“id”: “103748422”,
//“clientName”: “Joy”,
//“products”:[{“idProduct”: "1", “quantity”: "150"}]
// //}