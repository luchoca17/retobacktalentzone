package com.example.demo.dto;

import com.example.demo.models.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DTOBuy {
    private Long  buyId;
    private LocalDateTime date;
    private String clientName;
    private List<Product> products;
}