package com.example.demo.services;

import com.example.demo.appUtils.AppUtils;
import com.example.demo.dto.DTOProduct;
import com.example.demo.interfaces.IProduct;
import com.example.demo.models.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class ProductService implements IProduct {

    @Autowired
    ProductRepository productRepository;

    @Override
    public DTOProduct createProduct(DTOProduct dtoProduct) {
        Product savedProduct = productRepository.save(AppUtils.dtoToProduct(dtoProduct));
        return AppUtils.productToDto(savedProduct);
    }

    @Override
    public List<DTOProduct> findAllProducts() {

        return productRepository.findAll().stream().map(AppUtils::productToDto).toList();
    }

    @Override
    public DTOProduct updateProduct(DTOProduct dtoProduct, Long productId) {
        var product = productRepository.findById(productId);
        if (product.isPresent()) {
            dtoProduct.setProductId(productId);
            Product updatedProduct = productRepository.save(AppUtils.dtoToProduct(dtoProduct));
            return AppUtils.productToDto(updatedProduct);
        }
        return null;
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);

    }
}
