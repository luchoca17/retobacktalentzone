# RetoBackTalentZone

## Getting started

Para comenzar el proyecto se recomienda crear una base de datos en MySQL con el nombre "carritoCompras" para cuando se corra el proyecto
se creen las tablas correspondientes.

## Swagger

Para visualizar y probar los endpoints de la aplicacion se recomienda ingresar a este link http://localhost:8080/swagger-ui/#/
luego de correr la aplicacion.

## Postamn

Aca se encuentran los diferntes endopint que se crearon y se probaron en POSTMAN

![image info](assets/Postman.png) 
A continuacion se visualizan los Bodys de los endpoint de tipo POST para crear Productos, Clientes y las Compras respectivamente


![image info](assets/Postman_POSTproduct.png)

![image info](assets/Postman_POSTuser.png)

![image info](assets/Postman_POSTbuy.png) 