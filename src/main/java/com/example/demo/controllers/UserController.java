package com.example.demo.controllers;

import com.example.demo.dto.DTOUser;
import com.example.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    UserService userService;

    @PostMapping("/createUser")
    public ResponseEntity<DTOUser> createUser(@RequestBody DTOUser dtoUser){
        return ResponseEntity.ok().body(userService.createUser(dtoUser));
    }

    @GetMapping("/findAllUsers")
    public ResponseEntity<List<DTOUser>> findAllUsers(){
        return ResponseEntity.ok().body(userService.findAllUser());
    }

    @PutMapping("/updateUser/{userId}")
    public ResponseEntity<DTOUser> updateUser(
            @RequestBody DTOUser dtoUser,
            @PathVariable Long userId){
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(userService.updateUser(dtoUser,userId));
    }

    @DeleteMapping("/delete/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable("userId")Long userId){
        userService.deleteUser(userId);
        return null;
    }

}
