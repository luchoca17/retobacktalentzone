package com.example.demo.appUtils;

import com.example.demo.dto.DTOBuy;
import com.example.demo.dto.DTOProduct;
import com.example.demo.dto.DTOUser;
import com.example.demo.models.Buy;
import com.example.demo.models.Product;
import com.example.demo.models.User;
import org.springframework.beans.BeanUtils;

public class AppUtils {


    // PASAR DE MODELO A DTO

    public static DTOUser userToDto(User user) {
        DTOUser dtoUser = new DTOUser();
        BeanUtils.copyProperties(user, dtoUser);
        return dtoUser;
    }

    public static DTOProduct productToDto(Product product) {
        DTOProduct dtoProduct = new DTOProduct();
        BeanUtils.copyProperties(product, dtoProduct);
        return dtoProduct;
    }

    public static DTOBuy buyToDto(Buy buy) {
        DTOBuy dtoBuy = new DTOBuy();
        BeanUtils.copyProperties(buy, dtoBuy);
        return dtoBuy;
    }



    // PASAR DE DTO A MODELO

    public static User dtoToUser(DTOUser dtoUser) {
        User user = new User();
        BeanUtils.copyProperties(dtoUser, user);
        return user;
    }

    public static Product dtoToProduct(DTOProduct dtoProduct) {
        Product product = new Product();
        BeanUtils.copyProperties(dtoProduct, product);
        return product;
    }

    public static Buy dtoToBuy(DTOBuy dtoBuy) {
        Buy buy = new Buy();
        BeanUtils.copyProperties(dtoBuy, buy);
        return buy;
    }

}
